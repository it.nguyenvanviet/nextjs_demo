import Link from 'next/link';
import classNames from 'classnames/bind';
import styles from './Button.module.scss'; 
import React, { ComponentProps, ElementType, forwardRef, ForwardedRef }  from 'react';


const cx = classNames.bind(styles);


type ButtonProps<T extends ElementType> = {
    to?: string;
    href?: string;
    primary?: boolean;
    outline?: boolean;
    text?: boolean;
    small?: boolean;
    large?: boolean;
    rounded?: boolean;
    disabled?: boolean;
    children?: React.ReactNode;
    className?: string;
    leftIcon?: React.ReactNode;
    RightIcon?: React.ReactNode;
    onClick?: () => void;
    
} & ComponentProps<T>;


const ButtonInner = <T extends ElementType>({
    to,
    href,
    primary = false,
    outline = false,
    text = false,
    small = false,
    large = false,
    rounded = false,
    disabled = false,
    children  ,
    className = '',
    leftIcon,
    RightIcon,
    onClick, 
    ...passProps
}: ButtonProps<T>,  ref: ForwardedRef<HTMLLinkElement>): JSX.Element => {
    let Comp: any = 'button';
    interface BProps {
        [key: string]: any;
    }
    const props: BProps = {
        onClick,
        ...passProps,
    };
    if (disabled) {
        Object.keys(props).forEach((key) => {
            if (key.startsWith('on') && typeof props[key] === 'function') {
                delete props[key];
            }
        });
    }
    if (to) {
        props.href = to;
        Comp = Link;
    } else if (href) {
        props.href = href; 
        Comp = 'a';
    }
    const classes = cx('wrapper', {
        [className]: className,
        primary,
        outline,
        text,
        rounded,
        disabled,
        small,
        large,
    }); 
    return (
        <Comp className={classes} {...props} ref = {ref}>
            <span className={cx('title')}>
            {leftIcon && <span className={cx('icon')}>{leftIcon}</span>}    
            {children}
            {RightIcon && <span className={cx('icon')}>{RightIcon}</span>}
            </span>
        </Comp>
    );
};

export const Button = forwardRef(ButtonInner);
export default Button;
