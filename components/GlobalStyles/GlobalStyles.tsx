import React, { ComponentProps, ElementType } from 'react';


type GlobalStylesProps<T extends ElementType> = {
    children?: React.ReactNode;
} & ComponentProps<T>;

const GlobalStyles = <T extends ElementType = 'div'>({ children }: GlobalStylesProps<T>): JSX.Element => {
    return <div>{ children }</div>
};
export default GlobalStyles;

 