import React, { ForwardedRef, forwardRef, useState } from 'react';
import listimages from '@/components/assets/images';
import styles from './Image.module.scss';
import classNames from 'classnames';
import Image from 'next/image';

export type Ref = HTMLImageElement;
declare const VALID_LAYOUT_VALUES: readonly ["fill", "fixed", "intrinsic", "responsive", undefined];
declare type LayoutValue = typeof VALID_LAYOUT_VALUES[number];

interface ImageProps {
    className?: string;
    width?:string;
    height?:string;
    layout :LayoutValue;
    src: string;
    alt?: string;
    fallback?: any;
    customFallback?: any; 
}

function ImageInner(props: ImageProps, ref: ForwardedRef<HTMLImageElement>) {
    const { fallback: customFallback = listimages.noImages, ...AnotherProp } = props;
    const [fallBack, setFallBack] = useState('');
    const handleError = () => {
        setFallBack(customFallback);
    };
    return (
        <Image 
            {...AnotherProp}
            src={fallBack || props.src}
            className={classNames(styles.wrapper, props.className)} 
            width={props.width}
            height={props.height}
            layout={ props.layout}
            alt={props.alt}
            onError={handleError}
            priority={true}
        />
    );
}

export const ImageComp = forwardRef(ImageInner);

export default ImageComp;
