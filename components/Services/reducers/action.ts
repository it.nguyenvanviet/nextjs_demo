import { ADD_JOB, CHECK_JOB, DELETE_JOB, SET_JOB } from "./todoReducer"


// todo Action
export function setJob (payload: string) {
    return {
        type: SET_JOB,
        payload
    }
}
export function addJob (payload: string) {
    return {
        type: ADD_JOB,
        payload
    }
}
export function deleteJob (payload: string) {
    return {
        type: DELETE_JOB,
        payload
    }
}
export function checkJob (payload: string) {
    return {
        type: CHECK_JOB,
        payload
    }
}