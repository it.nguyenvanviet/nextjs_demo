import { generateUUID } from '@/lib/helpers/stringUtils';

// initState
export interface initialState {
    job: string;
    jobs: any;
}

// Action of todo reducer
export const SET_JOB = 'set_job';
export const ADD_JOB = 'add_job';
export const DELETE_JOB = 'delete_job';
export const CHECK_JOB = 'check_job';

// Reducer
export const todoReducer = (state: initialState, action: { type: any; payload: string }): initialState => {
    switch (action.type) {
        case SET_JOB:
            return { ...state, job: action.payload };
        case ADD_JOB:
            const newItem = { name: action.payload, checked: false, id: generateUUID() };
            // check exists local storage item
            const localJobs = localStorage.getItem('jobs');
            if (localJobs) {
                const jobs = JSON.parse(localJobs);
                jobs.push(newItem);
                localStorage.setItem('jobs', JSON.stringify(jobs));
            } else {
                localStorage.setItem('jobs', JSON.stringify([newItem]));
            }
            return { ...state, jobs: [...state.jobs, newItem] };
        case DELETE_JOB:
            // delete job in local storage
            const jobs = localStorage.getItem('jobs');
            if (jobs) {
                const newJobs = JSON.parse(jobs).filter((job: any) => job.id !== action.payload);
                console.log(newJobs);
                localStorage.setItem('jobs', JSON.stringify(newJobs));
            }
            return { ...state, jobs: state.jobs.filter((job: any) => job.id !== action.payload) };
        case CHECK_JOB:
            // check job in local storage
            const jobsCheck = localStorage.getItem('jobs');
            
            if (jobsCheck) {
                const newJobs = JSON.parse(jobsCheck).map((job: any) => {
                    if (job) {
                        if (job.id === action.payload) {
                            job.checked = !job.checked;
                        }
                    }
                    // console.log(job);
                    return job;
                });
                localStorage.setItem('jobs', JSON.stringify(newJobs));
            }
            return {
                ...state,
                jobs: state.jobs.map((job: any) => {
                    if (job) {
                        if (job.id === action.payload) {
                            job.checked = !job.checked;
                        }
                    }
                    return job;
                }),
            };

        default:
            return state;
    }
};
