import {
    Home,
    Box,
    DollarSign,
    Tag,
    Clipboard,
    Camera
} from 'react-feather';
 
const routes = [
    {
        path: '/dashboard', title: 'Dashboard', icon: Home, type: 'link', badgeType: 'primary', active: false, children:[]
    },
    {
        title: 'Products', path:'products', icon: Box, type: 'sub', active: false, children: [
            {
                title: 'Physical',path:'physical', type: 'sub', active: false, children: [
                    { path: '/products/physical/physical-category', title: 'Category', type: 'link', active: false, children: [] },
                    { path: '/products/physical/sub-category', title: 'Sub Category', type: 'link', active: false, children: [] },
                    { path: '/products/physical/product-list', title: 'Product List', type: 'link', active: false, children: [] },
                    { path: '/products/physical/product-detail', title: 'Product Detail', type: 'link', active: false, children: [] },
                    { path: '/products/physical/add-product', title: 'Add Product', type: 'link', active: false, children: [] },
                ]
            },
            {
                title: 'digital',path:'digital', type: 'sub', active: false, children: [
                    { path: '/products/digital/digital-category', title: 'Category', type: 'link', active: false, children: [] },
                    { path: '/products/digital/digital-sub-category', title: 'Sub Category', type: 'link', active: false, children: [] },
                    { path: '/products/digital/digital-product-list', title: 'Product List', type: 'link', active: false, children: [] },
                    { path: '/products/digital/digital-add-product', title: 'Add Product', type: 'link', active: false, children: [] },
                ]
            },
        ]
    },
    {
        title: 'Sales', path:'sales', icon: DollarSign, type: 'sub', active: false, children: [
            { path: '/sales/orders', title: 'Orders', type: 'link', active: false, children: [] },
            { path: '/sales/transactions', title: 'Transactions', type: 'link', active: false, children: [] },
        ]
    },
    {
        title: 'Coupons', path:'icon', icon: Tag, type: 'sub', active: false, children: [
            { path: '/coupons/list-coupons', title: 'List Coupons', type: 'link', active: false, children: [] },
            { path: '/coupons/create-coupons', title: 'Create Coupons', type: 'link', active: false, children: [] },
        ]
    },
    {
        title: 'Pages', path:'pages' , icon: Clipboard , type: 'sub', active: false, children: [
            { path: '/pages/list-page', title: 'List Page', type: 'link', active: false, children: [] },
            { path: '/pages/create-page', title: 'Create Page', type: 'link', active: false, children: [] },
        ]
    },
    {
        title: 'Media', path: '/media', icon: Camera, type: 'link', active: false, children:[]
    }, 
]

export default routes;
