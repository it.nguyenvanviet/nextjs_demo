export const FACEBOOK = {
    name: 'facebook',
    appId: '1234567890',
    version: 'v2.8',
    permissions: ['public_profile', 'email'],
    urlImage: 'scontent.fsgn2-1.fna.fbcdn.net',
};

export const GOOGLE = {
    name:'google',
    appId: '1234567890',
    version: 'v2.8',
    permissions: ['public_profile', 'email'],
    urlImage: 'lh3.googleusercontent.com',
};
