import { ChartOptions } from "chart.js";

export const lineOptions: ChartOptions | any = {  // options for line chart
    maintainAspectRatio: false, 
    animation: false,
    // legend: {
    //     display: false,
    // }, 
    elements: {
      point: {
        radius: 1
      }
    },
    barThickness: 10,
    scales:  { 
        x: {
          grid: {
            lineWidth: 1,
          }
        }
        // xAxes: [{
        //     barPercentage: 0.7,
        //     categoryPercentage: 0.4,
        //     gridLines: {
        //         display: false,
        //     },
        // }],
        // yAxes: [{
        //     barPercentage: 0.7,
        //     categoryPercentage: 0.4 
        // }]
    }
};
