import { useSession } from "next-auth/react";
import { type } from "os";
import { useCallback } from "react";
import { FACEBOOK, GOOGLE } from "../configs/social";

// Function to any domain like 'scontent.fsgn2-1.fna.fbcdn.net' domain to scontent.fsgn2-1.fna.fbcdn.net
export function ReplaceDomainFacebookImage(url: any) {
    let domainReplace = '';
    if(url) {
        let domain = '';
        domain = url.replace('https://', '').replace('http://', '').split(/[/?#]/)[0];
        domainReplace = url.replace(domain, FACEBOOK.urlImage);
    }
    return domainReplace;
}

// Function to any domain like 'scontent.fsgn2-1.fna.fbcdn.net' domain to scontent.fsgn2-1.fna.fbcdn.net
export function ReplaceDomainGoogleImage(url: any) {
    let domainReplace = '';
    if(url) {
        let domain = '';
        domain = url.replace('https://', '').replace('http://', '').split(/[/?#]/)[0];
        domainReplace = url.replace(domain, GOOGLE.urlImage);
    }
    return domainReplace;
}

export function generateUUID() { // Public Domain/MIT
    var d = new Date().getTime();//Timestamp
    var d2 = ((typeof performance !== 'undefined') && performance.now && (performance.now()*1000)) || 0;//Time in microseconds since page-load or 0 if unsupported
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = Math.random() * 16;//random number between 0 and 16
        if(d > 0){//Use timestamp until depleted
            r = (d + r)%16 | 0;
            d = Math.floor(d/16);
        } else {//Use microseconds since page-load if supported
            r = (d2 + r)%16 | 0;
            d2 = Math.floor(d2/16);
        }
        return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
    });
}

