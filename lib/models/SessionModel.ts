
export type SessionModel = {
    userRole: string;
    user: {
        name: string;
        email: string;
        image: string;
    };
    expires: string;
    provider : string;
};
 