// Layouts
import config from '@/lib/configs'; 
import Home from '@/pages/homepage';
import News from '@/pages/news';
;

// Publish Routes
const publishRoutes = [
    { path: config.routes[0].path, components: Home }, 
    // { path: config.routes.upload, components: Upload, layout: HeaderOnly },
    // { path: config.routes.search, components: Search, layout: null },
];

const privateRoutes: never[] = [];

export { publishRoutes, privateRoutes };
