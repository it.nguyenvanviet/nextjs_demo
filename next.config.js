module.exports = {
    images: {
      domains: [
        'atomiks.github.io',
        'lh3.googleusercontent.com',  
        'scontent.fsgn2-1.fna.fbcdn.net', 
    ],
    }, 
    disableExperimentalFeaturesWarning: true,
    jsconfigPaths: true
  }