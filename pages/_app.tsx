import '@/components/GlobalStyles/GlobalStyles.scss';
import { AppProps } from 'next/app';  
import { SessionProvider } from "next-auth/react"  
import '@fontsource/roboto/300.css';
import '@fontsource/roboto/400.css';
import '@fontsource/roboto/500.css';
import '@fontsource/roboto/700.css';

// Use of the <SessionProvider> is mandatory to allow components that call
// `useSession()` anywhere in your application to access the `session` object.
export default function App({ Component, pageProps }: AppProps) {
  return (
    <SessionProvider session={pageProps.session} refetchInterval={0}> 
        <Component {...pageProps} /> 
    </SessionProvider>
  )
}