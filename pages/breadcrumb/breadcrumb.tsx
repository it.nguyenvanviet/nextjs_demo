import React from 'react';
import styles from './breadcrumb.module.scss'
import classNames from 'classnames/bind';
import { HomeIcon } from '@/components/Icons/Icons';
import Button from '@/components/Button';
import config from '@/lib/configs';
 

const cx=  classNames.bind(styles);


const Breadcrumb = ({ title, parent   }: { title? :string, parent?: string }) => {
    return (
        <div className={cx('container-fluid')}>
            <div className={cx('page-header')}>
                <div className={cx('row')}>
                    <div className={cx('col-lg-6')}>
                        <div className={cx('page-header-left')}>
                            <h3> { title} <small> Admin panel</small> </h3>
                        </div>
                    </div>
                    <div className={cx('col-lg-6')}>
                        <ol className={cx('breadcrumb','pull-right')}>
                            <li className={cx('breadcrumb-item')}>
                                <Button to={config.routes[0].path} >
                                    <a href={config.routes[0].path} > <HomeIcon /></a>
                                </Button>  
                            </li>
                            <li className={cx('breadcrumb-item')}>{ parent }</li>
                            <li className={cx('breadcrumb-item','active')} > { title } </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Breadcrumb