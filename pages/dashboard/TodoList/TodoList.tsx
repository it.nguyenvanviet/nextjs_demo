import { addJob, checkJob, deleteJob, setJob } from "@/components/Services/reducers/action";
import { initialState, todoReducer } from "@/components/Services/reducers/todoReducer";
import classNames from "classnames/bind";
import React, { useEffect, useReducer } from "react";
import styles from './TodoList.module.scss';

const cx = classNames.bind(styles);

const TodoList = () => {
     // config for todo list
     const initialStateTodo: initialState = {
        job: '',
        jobs: []
    };
    const [state, dispatch] = useReducer(todoReducer, initialStateTodo);
    useEffect(() => {
        // Perform localStorage action
        initialStateTodo.jobs = JSON.parse(localStorage.getItem('jobs') || '[]'); 
    }, []);  

    const { job, jobs } = state;
    const inputRef = React.useRef() as React.MutableRefObject<HTMLInputElement>;

    const handleSubmitAddJob = (e: any) => {
        e.preventDefault();
        dispatch(addJob(job)); 
        dispatch(setJob(''));
        inputRef.current.focus();
    };
    const handleOnKeyDown = (e: React.KeyboardEvent<HTMLInputElement>) => {
        if (e.key === 'Enter') {
            handleSubmitAddJob(e);
        }
    };


    const handleClickCheckJob = (id: string) => {
        dispatch(checkJob(id));
        const checkbox = document.getElementById(id) as HTMLInputElement;
        checkbox.checked = !checkbox.checked;
        // find class todo-name element parent and add class strike
        const todoName = document.querySelector(`.todo-name-${id}`) as HTMLElement;
        todoName.classList.toggle('strike');


    };
    const handleDeleteJob = (id: string) => {
        dispatch(deleteJob(id));
    }; 
       
    return (
        <div className="col">
            <form>
                <input 
                    ref={inputRef}
                    className={cx('form-control-plaintext', 'todo-input')}
                    value={job}
                    placeholder="Enter to do job ... "
                    onKeyDown={handleOnKeyDown}
                    onChange={(e) => {
                        dispatch(setJob(e.target.value));
                    }}
                />
                <button
                    type="button"
                    className={cx('btn', 'btn-primary','todo-submit')}
                    onClick={handleSubmitAddJob}
                >
                    Add job
                </button>
            </form>
            <div className={cx('todo-list')}>
                {jobs.map((job: any, index: number) => (
                    (job) ? (
                        <div
                            className={cx('d-block', 'todo-item')}
                            key={index}
                        >
                            <input
                                className={cx('checkbox_animated')}
                                id={job.id}
                                type="checkbox"
                                checked={job.checked}
                                onChange={() => handleClickCheckJob(job.id)}
                            />
                            <span 
                                className={cx(`todo-name-${job.id}`, job.checked ? 'strike' : '')} 
                                onClick={() => handleClickCheckJob(job.id)}
                            >
                                {job.name}
                            </span>
                            <span 
                                className={cx('close')} 
                                onClick={() => handleDeleteJob(job.id)} 
                            > × </span>
                        </div>
                    ) 
                    : null  )) }
            </div>
        </div>   
    );
};

export default TodoList;
  
