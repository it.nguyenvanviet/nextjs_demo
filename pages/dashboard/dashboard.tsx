import React from 'react';
import DefaultLayout from '../layouts';
import { Bar } from 'react-chartjs-2';
import { Chart, CategoryScale, LinearScale, BarElement } from 'chart.js';
import { CommentIcon, CubeIcon, PaperPlaneIcon, PeopleIcon } from '@/components/Icons/Icons';
import Breadcrumb from '@/pages/breadcrumb';
import { lineOptions } from '@/lib/constants/charConfig';
import classNames from 'classnames/bind';
import styles from './dashboard.module.scss';
import WidgetCard from './widgetCard'; 
import TodoList from './TodoList';
import LastestOrders from './LastestOrders';
Chart.register(CategoryScale, LinearScale, BarElement);

const cx = classNames.bind(styles);

const Dashboard = () => {
    // data for chart
    const lineData = {
        labels: ['100', '200', '300', '400', '500', '600', '700', '800'],
        datasets: [
            {
                lagend: 'none',
                data: [2.5, 3, 3, 0.9, 1.3, 1.8, 3.8, 1.5],
                borderColor: '#ff8084',
                backgroundColor: '#ff8084',
                borderWidth: 2,
            },
            {
                lagend: 'none',
                data: [3.8, 1.8, 4.3, 2.3, 3.6, 2.8, 2.8, 2.8],
                borderColor: '#a5a5a5',
                backgroundColor: '#a5a5a5',
                borderWidth: 2,
            },
        ],
    };
    
   
    return (
        <DefaultLayout>
            <Breadcrumb title="Dashboard" parent="Dashboard" />
            <div className={cx('container-fluid')}>
                <div className={cx('row')}>
                    <div className={cx('col-xl-3', 'col-md-6', 'xl-50')}>
                        <WidgetCard
                            classnames="bg-warning"
                            title="Earnings"
                            counter="2000"
                            frequency="Monthly"
                            icon={<PaperPlaneIcon width="2.4rem" height="2.4rem" className="font-warning" />}
                        />
                    </div>
                    <div className={cx('col-xl-3', 'col-md-6', 'xl-50')}>
                        <WidgetCard
                            classnames="bg-secondary"
                            title="Products"
                            counter="2000"
                            frequency="Monthly"
                            icon={<CubeIcon width="2.4rem" height="2.4rem" className="font-secondary" />}
                        />
                    </div>
                    <div className={cx('col-xl-3', 'col-md-6', 'xl-50')}>
                        <WidgetCard
                            classnames="bg-primary"
                            title="Messages"
                            counter="2000"
                            frequency="Monthly"
                            icon={<CommentIcon width="2.4rem" height="2.4rem" className="font-primary" />}
                        />
                    </div>
                    <div className={cx('col-xl-3', 'col-md-6', 'xl-50')}>
                        <WidgetCard
                            classnames="bg-danger"
                            title="New customers"
                            counter="2000"
                            frequency="Monthly"
                            icon={<PeopleIcon width="2.4rem" height="2.4rem" className="font-danger" />}
                        />
                    </div>
                    <div className={cx('col-xl-6', 'xl-100')}>
                        <div className="card">
                            <div className="card-header">
                                <h5>Market Value</h5>
                            </div>
                            <div className="card-body">
                                <div className="market-chart">
                                    <Bar data={lineData} options={lineOptions} width={778} height={308} />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className={cx('col-xl-6', 'xl-100')}>
                        <div className="card">
                            <div className="card-header">
                                <h5>Todo</h5>
                            </div>
                            <div className="card-body">
                                <div className="market-chart">
                                    <TodoList />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className={cx('col-xl-6', 'xl-100')}>
                        <div className="card">
                            <div className="card-header">
                                <h5>Lastest Orders</h5>
                            </div>
                            <div className="card-body">
                                <div className="market-chart">
                                    <LastestOrders />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </DefaultLayout>
    );
};

export default Dashboard;
