import React from 'react';
import styles from './widgetCard.module.scss';
import classNames from 'classnames/bind';
const cx = classNames.bind(styles);

type WidgetCardProps = {
    title: string;
    icon : React.ReactNode;
    frequency : string;
    counter: string;
    classnames: string;
};
 

const WidgetCard = ( { title, icon, counter , frequency, classnames } : WidgetCardProps  ) => {
    const classes = cx('card-body', { [classnames]: classnames });
    return (
        <div className={cx('card', 'o-hidden', 'widget-cards')}>
            <div className={ classes }>
                <div className={cx('media','static-top-widget','row')}>
                    <div className={cx('icons-widgets','col-4')}>
                        <div className={cx('align-self-center','text-center')}>
                            {
                                // check if icon is a string or a react node
                                typeof icon === 'string' ? <img src={icon} alt={title} className={cx('img-fluid')} /> : icon 
                             }
                        </div>
                    </div>
                    <div className={cx('media-body','col-8')}>
                        <span className={cx('m-0', 'card-title')}>{ title }</span>
                        <h3 className={cx('mb-0')}>
                            $  <span className={cx('counter')}>{ counter } </span> <small> { frequency } </small>
                        </h3>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default WidgetCard;
