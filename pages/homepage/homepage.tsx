import React from 'react';
import styles from './home.module.scss'
import classNames from 'classnames/bind'; 
import Breadcrumb from '@/pages/breadcrumb'; 
import { CommentIcon, CubeIcon, PaperPlaneIcon, PeopleIcon } from '@/components/Icons/Icons';

const cx=  classNames.bind(styles);

const dataWidgetCard = [
    // create 4 item widget card
]

const HomePage = () => {
    return (
        <>
            <Breadcrumb title='Home Page' parent='Homepage' />
            <div> home</div>
        </>
    );
};



export default HomePage;
