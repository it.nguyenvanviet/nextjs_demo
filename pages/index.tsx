import React, { useState } from 'react';
import classNames from 'classnames/bind';
import Link from 'next/link';

import styles from '@/styles/login.module.scss';
import Button from '@/components/Button';
import config from '@/lib/configs';
import { FACEBOOK, GOOGLE } from '@/lib/configs/social'; 
import LayoutLogin from '@/pages/layouts/LayoutLogin';

import { Checkbox } from '@nextui-org/react';
import { getProviders, signIn } from 'next-auth/react';
import { FacebookIcon, GooglePlusIcon, LockOpenIcon, UserIcon } from '@/components/Icons/Icons';
import { faFacebookF, faGoogle } from '@fortawesome/free-brands-svg-icons';
import { IconProp } from '@fortawesome/fontawesome-svg-core';
import { GetServerSideProps, InferGetServerSidePropsType } from 'next';

const cx = classNames.bind(styles);
const faFB = faFacebookF as IconProp;
const faGg = faGoogle as IconProp;

const dataWidgetCard = [
    // create 4 item widget card
];
type Params = {
    params: {
        slug: string;
    };
};
const Login = ({ providers }: InferGetServerSidePropsType<typeof getServerSideProps>) => {
    const [tabIndex, setTabIndex] = useState(1);
    const [authorize, setAuthorize] = useState('');
    const handleSetTabIndex = (e: React.SetStateAction<number>) => { 
        setTabIndex(e);
    };
    const handleSetAuthorize = (e: string) => { 
        setAuthorize(e); 
        switch(e) {
            case FACEBOOK.name:
                signIn(providers.facebook.id); break;
            case GOOGLE.name:
                signIn(providers.google.id); break;
            default: break;
        }
    };
    return (
        <LayoutLogin>
            <div>
                <ul className={cx('tab', 'nav', 'nav-tabs', 'tab-coupon')}>
                    <li className={cx('nav-link', tabIndex === 1 ? 'active' : '')} onClick={() => handleSetTabIndex(1)}>
                        <UserIcon /> Login
                    </li>
                    <li className={cx('nav-link', tabIndex === 2 ? 'active' : '')} onClick={() => handleSetTabIndex(2)}>
                        <LockOpenIcon /> Register
                    </li>
                </ul>
                <div>
                    {tabIndex === 1 && (
                        <div>
                            <form className={cx('form-horizontal', 'auth-form')}>
                                <div className={cx('form-group')}>
                                    <input
                                        name="login[username]"
                                        type="email"
                                        className={cx('form-control')}
                                        placeholder="Username"
                                        id="exampleInputEmail1"
                                    />
                                </div>
                                <div className={cx('form-group')}>
                                    <input
                                        name="login[password]"
                                        type="password"
                                        className={cx('form-control')}
                                        placeholder="Password"
                                    />
                                </div>
                                <div className={cx('form-terms')}>
                                    <div className={cx('custom-control', 'custom-checkbox', 'mr-sm-2')}>
                                        <input
                                            type="checkbox"
                                            className={cx('custom-control-input')}
                                            id="customControlAutosizing"
                                        />
                                        <label className={cx('d-block')}>
                                            <Checkbox defaultSelected={true} size="sm" color="error">
                                                Remember Me
                                            </Checkbox>
                                            <span className={cx('pull-right')}>
                                                <Link href="/account/lostpassword">
                                                    <a
                                                        href="/account/lostpassword"
                                                        className={cx('btn', 'btn-default', 'forgot-pass', 'p-0')}
                                                    >
                                                        lost your password
                                                    </a>
                                                </Link>
                                            </span>
                                        </label>
                                    </div>
                                </div>
                                <div className={cx('form-button')}>
                                    <Button to={config.routes[0].path}>
                                        <a className={cx('btn', 'btn-primary')}> Login </a>
                                    </Button>
                                </div>
                                <div className={cx('form-footer')}>
                                    <span>Or Login up with social platforms</span>
                                    <ul className={cx('social')}>
                                        {providers && (
                                            <>
                                                {providers.facebook && (
                                                    <li>
                                                        <a onClick={() => { handleSetAuthorize(FACEBOOK.name); }} >
                                                            <FacebookIcon width="1rem" height="1rem" />
                                                        </a>
                                                    </li>
                                                )}
                                                {providers.google && (
                                                    <li>
                                                        <a onClick={() => { handleSetAuthorize(GOOGLE.name); }} >
                                                            <GooglePlusIcon width="1rem" height="1rem" />
                                                        </a>
                                                    </li>
                                                )}
                                            </>
                                        )}
                                    </ul>
                                </div>
                            </form>
                        </div>
                    )}
                    {tabIndex === 2 && <div> tab 2</div>}
                </div>
            </div>
        </LayoutLogin>
    );
};
export default Login;

export const getServerSideProps: GetServerSideProps = async (context) => {
    const providers = await getProviders();
    return {
        props: { providers },
    };
};
