import Head from 'next/head';
import Header from '@/pages/layouts/components/Header';
import styles from './DefaultLayout.module.scss';
import classNames from 'classnames/bind';
import Sidebar from '../components/Sidebar'; 

export const siteTitle = 'Next.js Sample Website';
const cx = classNames.bind(styles);

type Props = {
    children: React.ReactNode;
    home?: boolean; // This is a custom property
};

export default function DefaultLayout({ children  }: Props) {
    return (
        <div className={styles.container}>
            <Head>
                <link rel="icon" href="/favicon.ico" />
                <meta name="description" content="Learn how to build a personal website using Next.js" />
                <meta
                    property="og:image"
                    content={`https://og-image.vercel.app/${encodeURI(
                        siteTitle,
                    )}.png?theme=light&md=0&fontSize=75px&images=https%3A%2F%2Fassets.zeit.co%2Fimage%2Fupload%2Ffront%2Fassets%2Fdesign%2Fnextjs-black-logo.svg`}
                />
                <link
                    href="https://fonts.googleapis.com/css?family=Work+Sans:100,200,300,400,500,600,700,800,900"
                    rel="stylesheet"
                ></link>
                <link
                    href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
                    rel="stylesheet"
                ></link>
                <meta name="og:title" content={siteTitle} />
                <meta name="twitter:card" content="summary_large_image" />
            </Head>
            <Header />
            <div className={cx('wrapper')}>
                <Sidebar />
                <div className={cx('content')}>{children}</div>
            </div>
            <div>
                <footer className="footer">
                    <div className="container-fluid">
                        <div className="row">
                            <div className="col-md-6 footer-copyright">
                                <p className="mb-0">Copyright 2019 © Multikart All rights reserved.</p>
                            </div>
                            <div className="col-md-6">
                                <p className="pull-right mb-0">
                                    Hand crafted &amp; made with<i className="fa fa-heart"></i>
                                </p>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
    );
}
