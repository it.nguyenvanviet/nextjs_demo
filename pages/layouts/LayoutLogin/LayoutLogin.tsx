import styles from './LayoutLogin.module.scss';
import classNames from 'classnames/bind';
import { ArrowLeft } from 'react-feather'; 
import config from '@/lib/configs';
import Button from '@/components/Button'; 
import Image from '@/components/Image';
import listimages from '@/components/assets/images';
import Slider from 'react-slick';
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

const cx = classNames.bind(styles);

export const siteTitle = 'Next.js Sample Website';

export default function LayoutLogin({ children, home }: { children: React.ReactNode; home?: boolean }) {
    var settings = {
        dots: true,
        infinite: true,
        speed: 500,
        arrows: false
    };
    return (    
        <div className={cx("page-wrapper")}>
            <div className={cx("authentication-box")}>
                <div className={cx("container","login-box")}>
                    <div className={cx("row")}>
                        <div className={cx("col-md-5","p-0","card-left")}>
                            <div className={cx("card","bg-primary")} style={{ backgroundImage: `url(${listimages.backGround})` }}> 
                                <div className={cx("svg-icon")}> 
                                    <Image   
                                        layout='intrinsic'
                                        width='130px'
                                        height='130px'
                                        src={ listimages.backGroundLogin } 
                                    />
                                </div>
                                <Slider className="single-item" {...settings}>
                                    <div>
                                        <div>
                                            <h3>Welcome to Multikart</h3>
                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy.</p>
                                        </div>
                                    </div>
                                    <div>
                                        <div>
                                            <h3>Welcome to Multikart</h3>
                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy.</p>
                                        </div>
                                    </div>
                                    <div>
                                        <div>
                                            <h3>Welcome to Multikart</h3>
                                            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy.</p>
                                        </div>
                                    </div>
                                </Slider >
                            </div>
                        </div>
                        <div className={cx("col-md-7","p-0","card-right")}>
                            <div className={cx("card","tab2-card")}>
                                <div className={cx("card-body")}>
                                    {children}
                                </div>
                            </div>
                        </div> 
                    </div>  
                    <Button to={config.routes[0].path} >
                        <a className={cx("btn","btn-primary","back-btn")}> <ArrowLeft /> Back  </a>
                    </Button> 
                </div>
            </div>
        </div>
    );
}
