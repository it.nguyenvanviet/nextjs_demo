import React, { useCallback } from 'react';
import { BarIcon } from '@/components/Icons';
import { Tooltip, Grid } from '@nextui-org/react';
import { useSession } from 'next-auth/react';
import { ExpandIcon, RingIcon, NotificationIcon } from '@/components/Icons/Icons';
import classNames from 'classnames/bind';
import styles from './Header.module.scss';
import Notification from './Notification';
import Search from '../Search';
import Button from '@/components/Button';
import Image from '@/components/Image';
import ProfileDropDown from './ProfileDropDown';
import listimages from '@/components/assets/images';
import { authOptions } from '@/pages/api/auth/[...nextauth]';
import { unstable_getServerSession } from 'next-auth/next';
import { IncomingMessage, ServerResponse } from 'http';
import { NextApiRequest, NextApiResponse } from 'next';
import { ReplaceDomainFacebookImage, ReplaceDomainGoogleImage } from '@/lib/helpers/stringUtils';
import { FACEBOOK, GOOGLE } from '@/lib/configs/social';
import { SessionModel } from '@/lib/models/SessionModel';

const cx = classNames.bind(styles);
 
const Header = () => { 
    const { data: session, status } = useSession();
    const sessionModel = session as SessionModel;
    const handleGetImageSocial = useCallback((provider: string) => {
        switch(provider) {
            case FACEBOOK.name:
                return ReplaceDomainFacebookImage(sessionModel.user?.image);
            case GOOGLE.name:
                return ReplaceDomainGoogleImage(sessionModel.user?.image);
            default: return '';
        }
    }, [session]);  
    return (
        <div className={cx('wrapper')}>
            <div className={cx('main-header')}>
                <div className={cx('main-header-right', 'row')}>
                    <div className={cx('mobile-sidebar')}>
                        <div className={cx('media-body text-right switch-sm')}>
                            <label className={cx('switch')}>
                                <a>
                                    <BarIcon />
                                </a>
                            </label>
                        </div>
                    </div>
                    <div className={cx('nav-right', 'col')}>
                        <ul className={cx('nav-menus')}>
                            <li>
                                <Search />
                            </li>
                            <li>
                                <Button href="#!">
                                    <ExpandIcon />
                                </Button>
                            </li>
                            <li>
                                <a href="#!">
                                    <h6>EN</h6>
                                </a>
                            </li>
                            <li>
                                <Tooltip rounded content={<Notification />} placement="bottomEnd" leaveDelay={500}>
                                    <RingIcon />
                                    <span
                                        className={cx(
                                            'badge',
                                            'badge-pill',
                                            'badge-primary',
                                            'pull-right',
                                            'notification-badge',
                                        )}
                                    >
                                        3
                                    </span>
                                    <span className={cx('dot')}></span>
                                </Tooltip>
                            </li>
                            <li>
                                <NotificationIcon />
                                <span className={cx('dot')}></span>
                            </li>
                            <li className={cx('onhover-dropdown')}>
                                <Grid xs={4} justify="center">
                                    <Tooltip rounded content={<ProfileDropDown />} placement="bottomEnd">
                                        <div className={cx('media', 'align-items-center')}>
                                            <Image
                                                layout="fixed"
                                                width="50px"
                                                height="50px"
                                                className={cx(
                                                    'align-self-center',
                                                    'pull-right',
                                                    'img-50',
                                                    'rounded-circle',
                                                    'blur-up',
                                                    'lazyloaded',
                                                )}
                                                src={ session ? handleGetImageSocial(sessionModel?.provider) : listimages.noImages }
                                                alt="header-user"
                                                fallback="https://static.fullstack.edu.vn/static/media/f8-icon.7ad2b161d5e80c87e516.png"
                                            />
                                            <div className={cx('dotted-animation')}>
                                                <span className={cx('animate-circle')}></span>
                                                <span className={cx('main-circle')}></span>
                                            </div>
                                        </div>
                                    </Tooltip>
                                </Grid>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    );
};

export async function getServerSideProps(context: {
    req: (IncomingMessage & { cookies: Partial<{ [key: string]: string }> }) | NextApiRequest;
    res: ServerResponse | NextApiResponse<any>;
}) {
    const session = await unstable_getServerSession(context.req, context.res, authOptions);

    if (!session) {
        return {
            redirect: {
                destination: '/',
                permanent: false,
            },
        };
    }

    return {
        props: {
            session,
        },
    };
}

export default Header;
