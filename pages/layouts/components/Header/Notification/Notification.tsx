import React,{ memo } from 'react';
import classNames from 'classnames/bind';
import styles from './Notification.module.scss';
import { DownloadIcon, OrderIcon, WarningIcon } from '@/components/Icons/Icons'; 
const cx = classNames.bind(styles);
const propsNotification = [
    { id: 2, icon: <OrderIcon />, title: 'Order', content: 'Order #123 has been placed', classNames : '' },
    { id: 1, icon: <DownloadIcon />, title: 'Download', content: 'Downloaded 1 new item' , classNames : 'txt-success' },
    { id: 3, icon: <WarningIcon />, title: 'Warning', content: 'Warning: Low disk space', classNames : 'txt-danger' },
    { id: 3, icon: <WarningIcon />, title: 'Warning', content: 'Warning: Low disk space',   },
];

const Notification =  ()=> {
    return (
        <div>
            <ul className={cx('notification-dropdown', 'onhover-show-div','p-0')}>
                <li>
                    Notification
                    <span className={cx('badge','badge-pill', 'badge-primary', 'pull-right')}>{ propsNotification.length }</span>
                </li>
                {
                    propsNotification.map((item, index) => (
                        <li key={index}>
                             <div className={cx('media')}>
                                <div className={cx('media-body')}>
                                    <h6 className={cx('mt-0', [item.classNames] )}>
                                        <span>{ item.icon }</span>
                                        { item.title }
                                    </h6>
                                    <p className={cx('mb-0')}>{ item.content }</p>
                                </div>
                            </div>
                        </li>
                    ))
                } 
                <li className={cx('txt-dark')}>
                    <a href="#">All</a> notification
                </li>
            </ul>
        </div>
    );
};

export default memo(Notification);
