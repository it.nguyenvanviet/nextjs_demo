import styles from './ProfileDropDown.module.scss';
import classNames from 'classnames/bind';
import Link from 'next/link';
import Button from '@/components/Button';
import { memo } from 'react';
import { signOut } from 'next-auth/react';

const cx = classNames.bind(styles);

const LIST_PROFILE_ITEM = [
    { name: 'Edit Profile', href: '/profile', type: 'link' },
    { name: 'Inbox', href: '/inbox', type: 'link' },
    { name: 'Lock Screen', href: '', functionName: 'clockScreen', type: 'function' },
    { name: 'Settings ', href: '/settings', type: 'link' },
    { name: 'Logout', href: '/profile', functionName: 'signOut', type: 'function' },
];

const onClockScreenHandler = () => {    
    console.log('clock screen');
}
const onSignOutHandler = (e: { preventDefault: () => void; }) => {
    e.preventDefault();
    signOut();
}
        
const ProfileDropDown = () => {
    return (
        <ul className={cx('profile-dropdown', 'onhover-show-div', 'profile-dropdown-hover')}>
            {LIST_PROFILE_ITEM.map((item, index) => (
                <li key={index}>
                    {item.type === 'function' ? 
                    (
                        <Button onClick={ item.functionName === 'clockScreen' ? onClockScreenHandler: onSignOutHandler  }> {item.name} </Button>
                    ) : (
                        <Link href={item.href.length > 0 ? item.href : ''}> 
                            <a href={item.href.length > 0 ? item.href : ''}>
                                {item.name}
                            </a>
                        </Link>
                        )
                    }
                </li>
            ))}
        </ul>
    );
};

export default memo(ProfileDropDown);
