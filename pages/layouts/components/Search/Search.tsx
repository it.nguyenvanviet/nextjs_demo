 
import classNames from 'classnames/bind';
import styles from './Search.module.scss';
import { SearchIcon } from '@/components/Icons/Icons'; 
const cx = classNames.bind(styles);

const Search = () => {
    return (
        <form className={cx('form-inline', 'search-form')}>
            <div className={cx('form-group')}>
                <input type="search" className={cx('form-control-plaintext')} placeholder="Search..." />
                <span className={cx('d-sm-none', 'mobile-search')}>
                    <SearchIcon />
                </span>
            </div>
        </form>
    );
};

export default Search;
