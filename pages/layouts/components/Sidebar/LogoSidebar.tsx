import styles from './Sidebar.module.scss';
import classNames from 'classnames/bind';
import Link from 'next/link';
import Image from '@/components/Image';
import listimages from '@/components/assets/images';
const cx = classNames.bind(styles);

const LogoSideBar = () => {
    return (
        <div className={cx('main-header-left', 'd-none', 'd-lg-block')}>
            <div className={cx('logo-wrapper')}> 
                <Link href="/">
                    <a href='/'>
                        <Image 
                            layout='fixed' 
                            width='180px' 
                            height='34px' 
                            src={listimages.logo} alt="Logo" 
                        />
                    </a>
                </Link>
            </div>
        </div>
    );
};

export default LogoSideBar;
