import styles from './Sidebar.module.scss';
import classNames from 'classnames/bind';
import UserSideBar from './UserSideBar';
import LogoSideBar from './LogoSidebar'; 
import config from '@/lib/configs'; 
import React from 'react';
import Link from 'next/link';
import { Icon } from 'react-feather';
import { UrlObject } from 'url';

const cx = classNames.bind(styles); 

export default  class Sidebar extends React.Component {
    state = { selectedPath: "1", mainmenu: config.routes };
    onItemSelection = (arg: { path: any; }, _e: any) => {
        this.setState({ selectedPath: arg.path });
    }; 
    
   
     
    setNavActive(item: { title: string; active: boolean;
         children:  { path: string; title: string; type: string; }[]; } | 
        { title: string; icon: Icon; type: string; active: boolean; 
            children:  { 
                title: string; type: string; active: boolean; children: { path: string; title: string; type: string; active: boolean }[]; 
            }[]; 
            path?: undefined; badgeType?: undefined; } | { path: string; title: string; type: string; active: boolean } ) 
    { 
        config.routes.filter((menuItem: { active: boolean; children: any[]; }) => {
            if (menuItem != item)
                menuItem.active = false
            if (menuItem.children && menuItem.children.includes(item)) {
                menuItem.active = true
            }
            if (menuItem.children) {
                menuItem.children.filter((submenuItems: { active: boolean; children: any[]; }) => {
                    if (submenuItems != item) {
                        submenuItems.active = false
                    }
                    if (submenuItems.children) {
                        submenuItems.children.map((childItem: { active: boolean; }) => {
                            childItem.active = false;
                        })
                        if (submenuItems.children.includes(item)) {
                            submenuItems.active = true
                            menuItem.active = true
                        }
                    }
                })
            }
        })
        item.active = !item.active

        this.setState({
            mainmenu: config.routes
        })


    }

    render () {
        const theme = {
            selectionColor: "#C51162"
        };

        const mainmenu =  this.state.mainmenu.map((item, index) => (
        <li key={index} className={cx(`${item.active ? 'active' : ''}`)}> 
            {(item.type === 'sub') ? 
                <a  
                    className={cx('menu-item', 'sidebar-header') }
                    onClick={() => this.setNavActive(item)}
                > 
                <item.icon /> <span> {item.title}</span></a>  
                : ''}
            {(item.type === 'link') ?
                <Link href={item.path != undefined ? item.path : ''}> 
                        <a href= {item.path} 
                            className={cx('menu-item', 'sidebar-header' , item.active ? 'active': '')} 
                            onClick={() => this.setNavActive(item)} 
                        > 
                            <item.icon /> 
                            <span> {item.title}</span>
                        </a>  
                </Link>
                : ''}
            { 
                item.children.length > 0 ?  
                ( 
                    <ul className={cx('sidebar-submenu')}>
                    {
                        item.children.map((child, index) =>   
                            <li key={index} className={ cx(child.children ? child.active ? 'active' : '' : '' )} >
                                {(child.type === 'sub') ? 
                                    <a onClick={() => this.setNavActive(child)}  >  
                                        {child.title} 
                                    </a>  
                                    : ''}
                                {(child.type === 'link') ?
                                    <Link href={ child.path }> 
                                        <a href={ child.path }  onClick={() => this.setNavActive(child)} className={cx(child.active ? 'active' : '') }>  
                                            {child.title}
                                        </a>  
                                    </Link>
                                    : ''} 
                                {  
                                    child.children ? 
                                    <ul className={cx('sidebar-submenu_2',  `${child.active ? 'menu-open' : 'active'}`)}>
                                        {
                                            child.children.map((child2, key2) => 
                                            (
                                                <li className={cx(child2.active ? 'active' : '')} key={key2} >
                                                    <Link href={ child2.path }>
                                                        <a href={ child2.path }  onClick={() => this.setNavActive(child2)}> { child2.title} </a>
                                                    </Link>
                                                </li>
                                            )  
                                        )}
                                    </ul>  
                                    : ''
                                }
                                
                            </li>
                        )  
                    }
                </ul>
                ) : '' 
            }
        </li>
    ))  
        return (
        <div className={cx('wrapper')}>
            <div className={cx('logo')}>
                <LogoSideBar />
            </div>
            <div className={cx('sidebar', 'custom-scrollbar')}>
                <UserSideBar />
                <ul className={cx("sidebar-menu")}>
                   { mainmenu }
                </ul>
            </div>
        </div>
        );
    }
};

