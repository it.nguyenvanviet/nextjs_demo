import styles from './Sidebar.module.scss';
import classNames from 'classnames/bind';
import Image from '@/components/Image';
import { useSession } from 'next-auth/react';
import { SessionModel } from '@/lib/models/SessionModel';
import { useCallback } from 'react';
import { ReplaceDomainFacebookImage, ReplaceDomainGoogleImage } from '@/lib/helpers/stringUtils';
import { FACEBOOK, GOOGLE } from '@/lib/configs/social';
 
const cx = classNames.bind(styles);


const UserSideBar = () => {
    const { data: session, status } = useSession();
    const sessionModel = session as SessionModel;
    const handleGetImageSocial = useCallback((provider: string) => {
        switch(provider) {
            case FACEBOOK.name:
                return ReplaceDomainFacebookImage(sessionModel.user?.image);
            case GOOGLE.name:
                return ReplaceDomainGoogleImage(sessionModel.user?.image);
            default: return '';
        }
    }, [session]);  
    return (
    <div className={cx('sidebar-user','text-center')} >
        <div>
            <Image 
                layout='fixed'
                width='90px'
                height='90px'
                className={cx('img-60','rounded-circle')} 
                src={ session ? handleGetImageSocial(sessionModel.provider) : require('@/components/assets/images/img-250x250.png').default.src } 
            />
        </div>
        <h6 className={cx('mt-3','f-14')}>
            { session ? sessionModel.user?.name : '' }
        </h6>
        <p >{ session ? sessionModel.userRole : '' }</p>
    </div>
    );
};

export default UserSideBar;
