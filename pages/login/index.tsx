import React, { useState } from 'react';
import styles from './login.module.scss';
import classNames from 'classnames/bind';
import Button from '@/components/Button';
import config from '@/lib/configs';
import { Checkbox } from '@nextui-org/react';
import { FacebookIcon, GooglePlusIcon, LockOpenIcon, UserIcon } from '@/components/Icons/Icons';
import Link from 'next/link';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFacebookF, faGoogle } from '@fortawesome/free-brands-svg-icons';
import { IconProp } from '@fortawesome/fontawesome-svg-core';
import LayoutLogin from '../layouts/LayoutLogin';
import { signIn } from 'next-auth/react';

const cx = classNames.bind(styles);
const faFB = faFacebookF as IconProp;
const faGg = faGoogle as IconProp;

const dataWidgetCard = [
    // create 4 item widget card
];

const Login = () => {
    const [tabIndex, setTabIndex] = useState(1);
    const handleSetTabIndex = (e: React.SetStateAction<number>) => {
        console.log(e);
        setTabIndex(e);
    };
    return (
        <LayoutLogin>
            <div>
                <ul className={cx('tab', 'nav', 'nav-tabs', 'tab-coupon')}>
                    <li className={cx('nav-link', tabIndex === 1 ? 'active' : '')} onClick={() => handleSetTabIndex(1)}>
                        <UserIcon /> Login
                    </li>
                    <li className={cx('nav-link', tabIndex === 2 ? 'active' : '')} onClick={() => handleSetTabIndex(2)}>
                        <LockOpenIcon /> Register
                    </li>
                </ul>
                <div>
                    {tabIndex === 1 && (
                        <div>
                            <form className={cx('form-horizontal', 'auth-form')}>
                                <div className={cx('form-group')}>
                                    <input
                                        name="login[username]"
                                        type="email"
                                        className={cx('form-control')}
                                        placeholder="Username"
                                        id="exampleInputEmail1"
                                    />
                                </div>
                                <div className={cx('form-group')}>
                                    <input
                                        name="login[password]"
                                        type="password"
                                        className={cx('form-control')}
                                        placeholder="Password"
                                    />
                                </div>
                                <div className={cx('form-terms')}>
                                    <div className={cx('custom-control', 'custom-checkbox', 'mr-sm-2')}>
                                        <input
                                            type="checkbox"
                                            className={cx('custom-control-input')}
                                            id="customControlAutosizing"
                                        />
                                        <label className={cx('d-block')}>
                                            <Checkbox defaultSelected={true} size="sm" color="error">
                                                Remember Me
                                            </Checkbox>
                                            <span className={cx('pull-right')}>
                                                <Link href="/account/lostpassword">
                                                    <a
                                                        href="/account/lostpassword"
                                                        className={cx('btn', 'btn-default', 'forgot-pass', 'p-0')}
                                                    >
                                                        lost your password
                                                    </a>
                                                </Link>
                                            </span>
                                        </label>
                                    </div>
                                </div>
                                <div className={cx('form-button')}>
                                    <Button to={config.routes[0].path}>
                                        <a className={cx('btn', 'btn-primary')}> Login </a>
                                    </Button>
                                </div>
                                <div className={cx('form-footer')}>
                                    <span>Or Login up with social platforms</span>
                                    <ul className={cx('social')}>
                                        <li>
                                            <Button to = { config.routes[0].path }>
                                                <a>
                                                    <FacebookIcon width="1rem" height="1rem" />
                                                </a>
                                            </Button>
                                        </li>
                                        <li>
                                            <a
                                                href="/api/auth/signin"
                                                onClick={(e) => {
                                                    e.preventDefault();
                                                    signIn();
                                                }}
                                            >
                                                <GooglePlusIcon width="1rem" height="1rem" />
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </form>
                        </div>
                    )}
                    {tabIndex === 2 && <div> tab 2</div>}
                </div>
            </div>
        </LayoutLogin>
    );
};

export default Login;
