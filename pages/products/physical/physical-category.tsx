import React, { ReactElement } from 'react';
import DefaultLayout from '@/pages/layouts';
import { Chart, CategoryScale, LinearScale, BarElement } from 'chart.js';
import { CommentIcon, CubeIcon, PaperPlaneIcon, PeopleIcon } from '@/components/Icons/Icons';
import Breadcrumb from '@/pages/breadcrumb';
import { lineOptions } from '@/lib/constants/charConfig';
import classNames from 'classnames/bind';
import styles from './physical.module.scss'; 
Chart.register(CategoryScale, LinearScale, BarElement);
const cx = classNames.bind(styles);

const PhysicalCategory = () => {
    return (
        <DefaultLayout>
            <Breadcrumb  parent="Product" title="Physical-Category"/>
            <div className={cx('container-fluid')}>
                <div className={cx('row')}>
                    Physical Category
                </div>
            </div>
        </DefaultLayout>
    );
};
 
export default PhysicalCategory;
