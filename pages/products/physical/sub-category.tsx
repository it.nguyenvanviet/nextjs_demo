import React from 'react';
import DefaultLayout from '@/pages/layouts';
import { Chart, CategoryScale, LinearScale, BarElement } from 'chart.js'; 
import Breadcrumb from '@/pages/breadcrumb'; 
import classNames from 'classnames/bind';
import styles from './physical.module.scss'; 
Chart.register(CategoryScale, LinearScale, BarElement);
const cx = classNames.bind(styles);

const PhysicalSubCategory = () => {
    return (
        <DefaultLayout>
            <Breadcrumb title="Sub Category" parent="Product Physical" />
            <div className={cx('container-fluid')}>
                <div className={cx('row')}>
                    Sub Category
                </div>
            </div>
        </DefaultLayout>
    );
};
 
export default PhysicalSubCategory;
