import React, { ReactElement } from 'react';
import DefaultLayout from '@/pages/layouts';
import { Chart, CategoryScale, LinearScale, BarElement } from 'chart.js';
import { CommentIcon, CubeIcon, PaperPlaneIcon, PeopleIcon } from '@/components/Icons/Icons';
import Breadcrumb from '@/pages/breadcrumb';
import { lineOptions } from '@/lib/constants/charConfig';
import classNames from 'classnames/bind';
import styles from './sale-order.module.scss'; 
Chart.register(CategoryScale, LinearScale, BarElement);
const cx = classNames.bind(styles);

const SalesOrder = () => {
    return (
        <DefaultLayout>
            <Breadcrumb parent="Sales" title="Order"/>
            <div className={cx('container-fluid')}>
                <div className={cx('row')}>
                    Sale Order
                </div>
            </div>
        </DefaultLayout>
    );
};
 
export default SalesOrder;
