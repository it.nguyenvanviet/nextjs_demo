import React, { ReactElement } from 'react';
import DefaultLayout from '@/pages/layouts';
import { Chart, CategoryScale, LinearScale, BarElement } from 'chart.js';
import { CommentIcon, CubeIcon, PaperPlaneIcon, PeopleIcon } from '@/components/Icons/Icons';
import Breadcrumb from '@/pages/breadcrumb';
import { lineOptions } from '@/lib/constants/charConfig';
import classNames from 'classnames/bind';
import styles from './sale-transaction.module.scss'; 
Chart.register(CategoryScale, LinearScale, BarElement);
const cx = classNames.bind(styles);

const SalesTransactions = () => {
    return (
        <DefaultLayout>
            <Breadcrumb parent="Sales" title="Transactions"/>
            <div className={cx('container-fluid')}>
                <div className={cx('row')}>
                    Sales  Transactions
                </div>
            </div>
        </DefaultLayout>
    );
};
 
export default SalesTransactions;
